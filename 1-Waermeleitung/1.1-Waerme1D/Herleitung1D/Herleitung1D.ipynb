{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Wärmeleitungsgleichung in 1D\n",
    "\n",
    "## Geometrie des Systems\n",
    "\n",
    "Betrachten Stab mit Länge $l=b-a$ in $x$-Richtung.\n",
    "\n",
    "<center>\n",
    "<img src=\"stab.JPG\" alt=\"Drawing\" style=\"width: 40%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- Mathematisch \"real\" 1D: Stab hat keine Querschnittsfläche\n",
    "- Experimentell \"quasi\" 1D: Stab hat Querschnittsfläche $A=const.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Gesucht ist die Temperaturverteilung $u(x)$ entlang des Stabes. $u(x)$ ist die \"gesuchte Funktion\" (Skalarfeld auf dem Gebiet $\\Omega = (a,b)$).\n",
    "\n",
    "Hier und im Folgenden wird die \"gesuchte Funktion\" immer mit $u$ bezeichnet, unabhängig davon, welche physikalische Größe (z.B. Temperatur, Auslenkung, Intensität, ...) gesucht wird."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Wollen nun eine allgemein gültige Gleichung aufstellen, die die physikalisch notwendigen Bedingungen an die gesuchte Funktion $u(x)$ zusammenfasst und für alle möglichen gesuchten Funktionen erfüllt werden muss. Diese Gleichung ist die Differenzialgleichung für die Wärmeleitung."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Physikalische Grundgesetze\n",
    "\n",
    "### 1. Energiezufuhr führt zu Temperaturerhöhung"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Gültig, sofern keine Arbeit verrichtet wird und keine Phasenumwandlungen stattfinden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Zufuhr der Wärmemenge $\\Delta Q$ zu einem Material der Masse $m = \\rho V$ führt zu einer Temperaturerhöhung um $\\Delta u$\n",
    "\n",
    "$$\n",
    "\\Delta Q = c_p \\rho V \\Delta u.\n",
    "$$\n",
    "\n",
    "Dabei ist $c_p$ die spezifisch Wärmekapazität $[c_p]=\\frac{\\rm{J}}{\\rm{kg K}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Temperaturunterschiede führen zu Wärmestrom\n",
    "\n",
    "Wärmestrom ist so gerichtet, dass er Temperaturunterschiede ausgleicht.\n",
    "\n",
    "Die Wärmestromdichte $q_S$ ist die Änderung der thermischen Energie über eine Grenzfläche eines Systems je Zeit und Fläche.\n",
    "\n",
    "<center>\n",
    "<img src=\"wärmestromdichte grenzfläche.JPG\" alt=\"Drawing\" style=\"width: 10%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- Mathematisch \"real\" 1D:\n",
    "$$\n",
    "q_S(x) = \\frac{Q_S(x)}{\\Delta t} \\quad \\rm{mit} \\quad [q_S]=\\frac{\\rm{J}}{\\rm{s}}=\\rm{W}\n",
    "$$.\n",
    "\n",
    "- Experimentell \"quasi\" 1D:\n",
    "$$\n",
    "q_S(x) = \\frac{Q_S(x)}{A \\Delta t} \\quad \\rm{mit} \\quad [q_S]=\\frac{\\rm{J}}{\\rm{m^2 s}}=\\frac{\\rm{W}}{\\rm{m^2}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Die Wärmestromdichte ist durch das Fouriersche Gesetz gegeben:\n",
    "\n",
    "$$\n",
    "q_S(x) = -k \\frac{du}{dx}(x)\n",
    "$$\n",
    "\n",
    "Dabei ist $k$ die Wärmeleitfähigkeit mit der Einheit $[k]=\\frac{\\rm{W}}{\\rm{K}}$ (für \"real\" 1D)  bzw.  $[k]=\\frac{\\rm{W}}{\\rm{m^2 K}}$ (für \"quasi\" 1D).\n",
    "\n",
    "Zu Notation: Die erste Ableitung der Funktion $u(x)$ nach dem Ort $x$ schreiben wir als $\\frac{du}{dx}(x) = u^\\prime(x)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Stationärer Fall, konstante Wärmeleitfähigkeit $k$\n",
    "\n",
    "Betrachten zunächst den stationären Fall, also Situationen ohne zeitliche Änderungen. Weiterhin soll die Wärmeleitfähigkeit $k$ des Materials konstant, also unabhängig vom Ort sein."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### 1. Herleitung der Differenzialgleichung für Wärmeleitung\n",
    "\n",
    "Betrachten nun einen Teil des Stabes der Länge $\\Delta x$:\n",
    "\n",
    "\n",
    "<center>\n",
    "<img src=\"wärmestromdichte volumen.JPG\" alt=\"Drawing\" style=\"width: 25%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Nach den physikalischen Grundgesetzen muss die Wärmestromdichte an den Grenzflächen $x$ und $x+\\Delta x$ gleich sein (sonst würde ja eine Temperaturänderung aus $\\Delta Q \\neq 0$ folgen)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Es muss also gelten $q_S(x+\\Delta x) = q_S(x)$.\n",
    "\n",
    "Damit folgt:\n",
    "\n",
    "\\begin{align}\n",
    "0 &= q_S(x+\\Delta x) - q_S(x)\\\\\n",
    " &= - k \\frac{du}{dx}(x+ \\Delta x) + k \\frac{du}{dx}(x)\\\\\n",
    " &= -k \\left( \\frac{du}{dx}(x+ \\Delta x) - \\frac{du}{dx}(x) \\right)\\\\\n",
    " &= -k \\left[ \\frac{du}{dx}(\\tilde{x}) \\right]_x^{x+\\Delta x}\\\\\n",
    " &= -k \\int\\limits_x^{x+\\Delta x} \\frac{d^2u}{dx^2}(\\tilde{x}) \\,d\\tilde{x}\\\\\n",
    " &= \\int\\limits_x^{x+\\Delta x} -k\\frac{d^2u}{dx^2}(\\tilde{x}) \\,d\\tilde{x}\\\\\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Die Gleichung muss für beliebige $x$ und $\\Delta x$ erfüllt sein. Daher muss der Integrand verschwinden und es muss nach Ersetzung von $\\tilde{x}$ durch $x$ gelten\n",
    "\n",
    "$$\n",
    "-k\\frac{d^2u}{dx^2}(x)=0\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Also gilt entweder $k = 0$ (keine Wärmeleitfähigkeit, also idealer Isolator) oder die zweite Ableitung der gesuchten Funktion $u(x)$ nach dem Ort muss an jedem Ort verschwinden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Wie sind wir bei der Herleitung der Differenzialgleichung für die Wärmeleitung vorgegangen?\n",
    "\n",
    "Wir haben den Hauptsatz der Differenzial- und Integralrechung (Berechnung eines Integrals als Differenz der Funktionswerte der Stammfunktion) genutzt:\n",
    "- Wir haben die bekannte Wärmestromdichte als Stammfunktion betrachtet.\n",
    "- Die Differenz der Wärmestromdichten haben wir als Integral über die Ableitung der Wärmestromdichten ausgedrückt.\n",
    "- Durch diese \"Integration rückwärts\" haben wir eine einfache Gleichung erhalten, die von allen gesuchten Funktionen für die Temperaturverteilung im stationären Fall $u(x)$ erfüllt sein muss."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Lösung der Differenzialgleichung für Wärmeleitung\n",
    "\n",
    "Suchen nun die unbekannte Funktion $u(x)$ die die Wärmeleitungsgleichung löst."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Ansatz: Integration der Differenzialgleichung für Wärmeleitung\n",
    "\n",
    "$$\n",
    "-k\\frac{d^2u}{dx^2}(x)=0,\n",
    "$$\n",
    "\n",
    "um von der Bedingung an die zweite Ableitung wieder zur gesuchten Funktion zu kommen. Durch diese \"Integration vorwärts\" machen wir die \"Integration rückwärts\" bei der Herleitung der Gleichung wieder rückgängig."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Für $k\\neq0$ muss also gelten\n",
    "\n",
    "\\begin{align}\n",
    "& \\frac{d^2u}{dx^2}(x)=0\\\\\n",
    "\\Rightarrow &  \\int\\limits_{x_0}^{x} \\frac{d^2u}{dx^2}(\\tilde{x}) \\,d\\tilde{x} = \\int\\limits_{x_0}^{x} 0 \\,d\\tilde{x}\\\\\n",
    "\\Rightarrow & \\left[ \\frac{du}{dx}(\\tilde{x}) \\right]_{x_0}^{x} = 0\\\\\n",
    "\\Rightarrow & \\frac{du}{dx}(x) - \\frac{du}{dx}(x_0) = 0\\\\\n",
    "\\Rightarrow & \\int\\limits_{x_0}^{x} \\frac{du}{dx}(\\tilde{x}) \\,d\\tilde{x} - \\int\\limits_{x_0}^{x} \\frac{du}{dx}(x_0) \\,d\\tilde{x} =  \\int\\limits_{x_0}^{x} 0 \\,d\\tilde{x}\\\\\n",
    "\\Rightarrow & \\left[ u(\\tilde{x}) \\right]_{x_0}^{x} - \\left[ \\frac{du}{dx}(x_0)\\cdot\\tilde{x} \\right]_{x_0}^{x} = 0\\\\\n",
    "\\Rightarrow & u(x) - u(x_0) - \\frac{du}{dx}(x_0) \\cdot x + \\frac{du}{dx}(x_0) \\cdot x_0 = 0\\\\\n",
    "\\Rightarrow & u(x) = \\underbrace{\\frac{du}{dx}(x_0)}_{m} \\cdot x + \\underbrace{u(x_0) - \\frac{du}{dx}(x_0) \\cdot x_0}_{b}\\\\\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Die unbekannte Funktion $u(x)$ ist also eine lineare Funktion der Form $u(x)=m\\cdot x+b$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Wählen zur Vereinfachung und ohne Beschränkung der Allgemeinheit $x_0=0$. Damit folgt\n",
    "$$\n",
    "u(x) = \\underbrace{\\frac{du}{dx}(0)}_{m} \\cdot x + \\underbrace{u(0)}_{b}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Die gesuchte Funktion $u(x)$ enthält also noch zwei Unbekannte, $m$ und $b$. Diese Unbekannten resultieren aus den Integrationskonstanten der zwei Integrationen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Für eine konkrete Lösung $u(x)$ werden diese beiden Unbekannten in Form von sogenannte Randbedingungen vorgegeben. Randbedingungen sind Bedingungen an die gesuchte Funktion auf dem Rand des Gebiets $\\Omega = (a,b)$.\n",
    "Typischerweise werden folgende Randbedingungen verwendet:\n",
    "- Dirichlet-Randbedingung: Es werden Funktionswerte der Funktion $u(x)$ auf dem Rand von $\\Omega$ vorgegeben (hier also bestimmte Temperaturen am Rand des Stabes).\n",
    "- Neumann-Randbedingung: Es werden Funktionswerte der Ableitung $\\frac{du}{dx}(x)$ auf dem Rand von $\\Omega$ vorgegeben (dies entspricht bestimmten Wärmeflüssen $q_S(x) = -k \\frac{du}{dx}(x)$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Stationärer Fall, ortsabhängige Wärmeleitfähigkeit $k(x)$, orstabhängiger Quellterm $q_Q(x)$\n",
    "\n",
    "Betrachten wieder den stationären Fall, also Situationen ohne zeitliche Änderungen.\n",
    "\n",
    "Allerdings soll die  Wärmeleitfähigkeit $k$ des Materials nun vom Ort abhängig sein können, also $k=k(x)$. Weiterhin berücksichtigen wir Wärmequellen in dem Stab, wobei die Wärmequelldichte $q_Q$ ebenfalls ortsabhängig sein kann, also $q_Q=q_Q(x)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Die Wärmequelldichte $q_Q$ ist die Änderung der thermischen Energie des Systems je Zeit und Volumeneinheit.\n",
    "\n",
    "<center>\n",
    "<img src=\"wärmequelldichte.JPG\" alt=\"Drawing\" style=\"width: 20%;\"/>\n",
    "</center>\n",
    "\n",
    "- Mathematisch \"real\" 1D:\n",
    "$$\n",
    "q_Q(x) = \\frac{Q_Q(x)}{\\Delta x \\cdot \\Delta t} \\quad \\rm{mit} \\quad [q_Q]=\\frac{\\rm{J}}{\\rm{m \\cdot s}}=\\frac{\\rm{W}}{m}\n",
    "$$.\n",
    "\n",
    "- Experimentell \"quasi\" 1D:\n",
    "$$\n",
    "q_Q(x) = \\frac{Q_Q(x)}{A \\cdot \\Delta x \\cdot \\Delta t} \\quad \\rm{mit} \\quad [q_Q]=\\frac{\\rm{J}}{\\rm{m^3 s}}=\\frac{\\rm{W}}{\\rm{m^3}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Ein Beispiel für eine solche Wärmequelle ist Joulsche Wärme infolge von Stromfluss durch einen elektrischen Leiter mit spezifischem Widerstand $\\rho(x)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 1. Herleitung der Differenzialgleichung für Wärmeleitung\n",
    "\n",
    "Betrachten wieder einen Teil des Stabes der Länge $\\Delta x$:\n",
    "\n",
    "<center>\n",
    "<img src=\"wärmequelldichte.JPG\" alt=\"Drawing\" style=\"width: 20%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Aufgrund der Energieerhaltung muss die Quellstärke in Volumen zwischen $x$ und $x+\\Delta x$ gerade der Differenz der Wärmeströme $Q_S$ an den Grenzflächen $x$ und $x+\\Delta x$ entsprechen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Damit muss gelten\n",
    "\n",
    "\\begin{align}\n",
    "& \\int\\limits_{x}^{x + \\Delta x} q_Q(\\tilde{x}) \\cdot A \\cdot \\Delta t \\,d\\tilde{x} &\\stackrel{!}{=}& Q_S(x + \\Delta x) - Q_S(x) = q_S(x + \\Delta x) \\cdot A \\cdot \\Delta t - q_S(x) \\cdot A \\cdot \\Delta t\\\\\n",
    "\\Rightarrow & \\int\\limits_{x}^{x + \\Delta x} q_Q(\\tilde{x}) \\,d\\tilde{x} &=& q_S(x + \\Delta x) - q_S(x)\\\\\n",
    "& &=& -k(x + \\Delta x) \\cdot \\frac{du}{dx}(x + \\Delta x) + k(x) \\cdot \\frac{du}{dx}(x)\\\\\n",
    "& &=& -\\left[ k(\\tilde{x}) \\cdot \\frac{du}{dx}(\\tilde{x}) \\right]_x^{x+\\Delta x}\\\\\n",
    "& &=& -\\int\\limits_x^{x+\\Delta x} \\frac{d \\left( k(\\tilde{x}) \\cdot \\frac{du}{dx}(\\tilde{x})\\right)}{dx}(\\tilde{x}) \\,d\\tilde{x}\\\\\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Die Gleichung muss wieder für beliebige $x$ und $\\Delta x$ erfüllt sein. Daher müssen die Integranden gleich sein, es muss nach Ersetzung von $\\tilde{x}$ durch $x$ also gelten\n",
    "\n",
    "$$\n",
    " -\\frac{d \\left( k(x) \\cdot \\frac{du}{dx}(x)\\right)}{dx}(x) = q_Q(x)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Diese Differenzialgleichung für Wärmeleitung stellt also schon eine komplexere Bedingung an die gesuchte Funktion für die Temperaturverteilung im stationären Fall $u(x)$ dar. Sie besagt, dass für $u(x)$ an jedem Ort $x$ folgendes gelten muss: die Ableitung vom Produkt aus $k(x)$ und $\\frac{du}{dx}(x)$ muss gleich der Wärmequelldichte $q_Q(x)$ sein."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Die physikalische Bedeutung dieser Gleichung wird offensichtlich, wenn wir den Ausdruck für die Wärmestromdichte $q_s(x)=k(x)\\cdot\\frac{du}{dx}(x)$ einsetzen. Dann muss gelten\n",
    "\n",
    "$$\n",
    " -\\frac{d q_s(x)}{dx}(x) = q_Q(x)\n",
    "$$\n",
    "\n",
    "Im stationären Fall muss einer Änderung der Wärmestromdichte entlang des Stabes also stets eine entsprechende Wärmequelldichte entsprechen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Für den Fall dass die Wärmeleitfähigkeit $k(x)$ unabhängig vom Ort $x$ ist, also $k(x)=k$ gilt, kann die Konstante $k$ vor die Ableitung gezogen werden. Dann gilt\n",
    "\n",
    "$$\n",
    "-\\frac{d \\left( k \\cdot \\frac{du}{dx}(x)\\right)}{dx}(x) = -k\\frac{d \\left(\\frac{du}{dx}(x)\\right)}{dx}(x) = -k \\frac{d^2u}{dx^2}(x) = q_Q(x)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Ist außerdem $q_Q(x)=0$ erhalten wir wieder die Differenzialgleichung für Wärmeleitung für den stationärer Fall mit konstanter Wärmeleitfähigkeit $k$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Lösung der Differenzialgleichung für Wärmeleitung\n",
    "\n",
    "####  Übung\n",
    "\n",
    "Wir betrachten einen Stab der Länge $l=b-a$, mathematisch also das eindimensionale Gebiet $\\Omega=(a:b)$. Der Stab habe eine konstante Wärmeleitfähigkeit $k$ und werde mit einer konstanten Wärmequelldichte $q_Q$ geheizt. Es ist also $q_Q>0$.\n",
    "\n",
    "- Berechnen Sie für diesen Stab aus der Differenzialgleichung für Wärmeleitung \n",
    "$$\n",
    " -k \\frac{d^2u}{dx^2}(x) = q_Q\n",
    "$$\n",
    "die Temperaturverteilung $u(x)$ für den stationärer Fall.\n",
    "\n",
    "\n",
    "- Zeigen Sie, dass sich die Temperaturverteilung im stationären Fall durch\n",
    "$$\\tag{*}\n",
    " u(x)=-\\frac{q_Q}{2k} \\cdot x^2 + \\frac{du}{dx}(0) \\cdot x + u(0)\n",
    "$$\n",
    "ausdrücken lässt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Geben Sie an, welche Ausdrücke in dieser Gleichung Materialparameter und welche Ausdrücke Randbedingungen (welche Art von Randbedingungen?) sind.\n",
    "\n",
    "\n",
    "- Betrachten Sie nun ein Experiment, in dem beide Randbedingungen Dirichlet-Randbedingungen sind, in dem also die Temperatur an den Endes des Stabes durch $u(a)$ und $u(b)$ vorgegeben ist. Bringen Sie Gleichung (*) in eine Form, in der neben den Materialparametern nur diese beiden Randbedingungen auftauchen.\n",
    "\n",
    "\n",
    "- Durch die Heizung des Stabes ist es möglich, dass das Temperaturmaximum des Stabes nicht an den Stabenden sondern dazwischen angenommen wird. Zeigen Sie, dass ein Temperaturmaxiumum zwischen den Stabenden nur unter der Bedingung\n",
    "$$\n",
    " \\left|u(b)-u(a)\\right|<\\frac{q_Ql^2}{2k}\n",
    "$$\n",
    "auftreten kann."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
