# XLAB physics course

Material for XLAB physics course        

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Fxlab-physics-camp/master?filepath=index.ipynb) (interactive jupyter notebooks)

[![lite-badge](https://jupyterlite.rtfd.io/en/latest/_static/badge.svg)](https://lehrenfeld.pages.gwdg.de/xlab-physics-camp/lab/index.html)
