import matplotlib.pyplot as plt
from numpy import nan, zeros, linspace
from IPython import display
from math import pi, cos, sin

def plot(timehistory, anglehistory, tangvelhistory, lengthhistory, radvelhistory):
    import matplotlib.pyplot as plt
    plt.figure(figsize=(10, 4))
    plt.plot(timehistory, anglehistory,'-o',label='Winkel')
    plt.plot(timehistory, tangvelhistory,'-*',label='Winkelgeschwindigkeit')
    plt.plot(timehistory, lengthhistory,'-o',label='Länge')
    plt.plot(timehistory, radvelhistory,'-*',label='Radialgeschwindigkeit')
    plt.xlabel("Zeit")
    plt.legend()
    plt.show()



def plot_position(timehistory, anglehistory, tangvelhistory, lengthhistory, radvelhistory,
                  x0 = 0, y0 = 0, r = 1):
    import matplotlib.pyplot as plt
    xcoords = zeros(len(anglehistory))
    ycoords = zeros(len(anglehistory))
    xcoords0 = zeros(len(anglehistory))
    ycoords0 = zeros(len(anglehistory))

    angle_samp = linspace(0,2*pi,100)
    xcoords_circ = zeros(len(angle_samp))
    ycoords_circ = zeros(len(angle_samp))
    for i, angle in enumerate(angle_samp):
        xcoords_circ[i] = x0 + cos(angle) * r
        ycoords_circ[i] = y0 + sin(angle) * r

    for i, (angle, length) in enumerate(zip(anglehistory, lengthhistory)):
        xcoords0[i] = x0 + cos(angle) * r
        ycoords0[i] = y0 + sin(angle) * r

        xcoords[i] = x0 + cos(angle) * r + sin(angle) * length
        ycoords[i] = y0 + sin(angle) * r - cos(angle) * length

    plt.figure(figsize=(10, 4))
    plt.plot(xcoords_circ, ycoords_circ,'-',label='Kreis')
    plt.plot(xcoords0, ycoords0,'-o',label='Kontakt am Kreis')
    plt.plot(xcoords, ycoords,'-o',label='Position')
    plt.legend()
    plt.show()


