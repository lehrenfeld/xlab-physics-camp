{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Pendel\n",
    "## Geometrie des Systems\n",
    "\n",
    "Betrachten ein Fadenpendel mit Länge $l$ an dem eine Masse $m$ angebracht ist. Das Pendel sei starr aufgehangen und soll nur in einer Ebene durch den Aufhängepunkt schwingen. Die Position des Pendels ist dann durch den Winkel $u$, um den das Pendel aus der Ruhelage ausgelenkt ist, eindeutig bestimmt. Insofern handelt es mathematisch um ein 1D-System.\n",
    "\n",
    "\n",
    "<center>\n",
    "<img src=\"fadenpendel.JPG\" alt=\"Drawing\" style=\"width: 20%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Wir suchen die Auslenkung des Pendels aus der Ruhelage $u$ in Abhängigkeit von der Zeit $t$. Die gesuchte Funktion $u(t)$ hat als möglichen Definitionsbereich das Gebiet $\\Omega = [-\\infty,\\infty]$.\n",
    "\n",
    "Um die Funktion $u(t)$ zu finden stellen wir zunächst wieder eine allgemein gültige Gleichung auf, die die physikalisch notwendigen Bedingungen an die gesuchte Funktion $u(t)$ zusammenfasst und für alle möglichen gesuchten Funktionen erfüllt werden muss. Diese Gleichung ist die Differenzialgleichung für das Fadenpendel."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Physikalische Grundgesetze\n",
    "### 1. Potentielle und kinetische Energie\n",
    "\n",
    "Wir wählen den Nullpunkt der potentielle Energie des Fadenpendels für die Auslenkung $u=0$. Die potentielle Energie für eine beliebige Auslenkung $u$ aus der Ruhelage ist dann\n",
    "\n",
    "$$\n",
    "E_{\\rm{pot}}(t)=mg\\left(l-l\\cos(u(t))\\right)=mgl\\left(1-\\cos(u(t))\\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Die kinetische Energie des Fadenpendels ist durch die Bahngeschwindigkeit $v$ bestimmt. Für die Bahngeschwindigkeit gilt $v=\\frac{du}{dt}(t)\\cdot l$. Damit folgt für die kinetische Energie\n",
    "$$\n",
    "E_{\\rm{kin}}=\\frac{1}{2}mv^2=\\frac{1}{2}m\\left(\\frac{du}{dt}(t)\\cdot l\\right)^2\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "[Anmerkung: Bei Kreisbewegungen wird häufig der Winkel mit $\\varphi$ und der Radius mit $r$ bezeichnet, für die Winkelgeschwindigkeit $\\omega$ gibt dann $\\omega(t)=\\frac{d\\varphi}{dt}(t)$ und für die Bahngeschwindigkeit somit $v(t)=\\omega(t) r=\\frac{d\\varphi}{dt}(t)\\cdot r$. Mit der Ersetzung $u=\\varphi$ und $r=l$ folgt die oben angegebene Gleichung für die Bahngeschwindigkeit.]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Energieerhaltung\n",
    "\n",
    "Gibt es keine Verluste, so ist die Gesamtenergie des Fadenpendels $E(t)=E_{\\rm{pot}}(t)+E_{\\rm{kin}}(t)$ konstant. Die Ableitung der Gesamtenergie nach der Zeit $t$ muss also verschwinden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "#### Übung:\n",
    "Es ist $E_{\\rm{pot}}(t)=mgl\\left(1-\\cos(u(t))\\right)$ und $E_{\\rm{kin}}(t)=\\frac{1}{2}m\\left(\\frac{du}{dt}(t)\\cdot l\\right)^2$. Bestimmen Sie aus der Bedingung $E=const.$ die Differenzialgleichung für das Fadenpendel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Es gilt also:\n",
    "\n",
    "$$\n",
    "\\frac{d^2u}{dt^2}(t) = -\\frac{g}{l}\\sin(u(t))\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Es gilt also:\n",
    "\\begin{eqnarray}\n",
    "0 & \\stackrel{!}{=} & \\frac{dE}{dt}(t)\\\\\n",
    "& = & mgl\\sin(u(t))\\frac{du}{dt}(t)+m\\frac{du}{dt}(t)\\frac{d^2u}{dt^2}(t)\\cdot l^2\\\\\n",
    "& = & ml\\frac{du}{dt}(t)\\cdot\\left( g\\sin(u(t))+\\frac{d^2u}{dt^2}(t)\\cdot l\\right)\\\\\n",
    "\\Rightarrow \\frac{d^2u}{dt^2}(t) & = & -\\frac{g}{l}\\sin(u(t))\\\\\n",
    "\\end{eqnarray}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Dies ist die Differenzialgleichung für das Fadenpendel. Diese nichtlineare Differenzialgleichung 2. Ordnung ist nicht einfach lösbar. Im Folgenden betrachten wir unterschiedliche Fälle.\n",
    "\n",
    "[Anmerkung: Formell gibt es noch die Lösungen $m=0$, $l=0$ und $\\frac{du}{dt}(t)=0$, also keine Änderung des Winkels $u$ mit der Zeit. Diese Lösung beschreibt ein nicht schwingendes Pendel.]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Harmonischer Oszillator\n",
    "\n",
    "### 1. Näherung der Differenzialgleichung für kleine Auslenkungen\n",
    "\n",
    "Die Differenzialgleichung für das Pendel lässt sich vereinfachen wenn nur kleine Auslenkungen aus der Ruhelage vorkommen. In dieser Kleinwinkelnäherung gilt $\\sin(u(t))\\approx u(t)$. Damit vereinfacht sich die Differenzialgleichung für das Fadenpendel zu\n",
    "\n",
    "$$\n",
    "\\frac{d^2u}{dt^2}(t) = -\\frac{g}{l}u(t)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Die ist die Differenzialgleichung für einen harmonischen Oszillator, man erhält sich auch für ein Federpendel das nur so stark ausgelenkt wird, dass das Hookesche Gesetz gilt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Lösung der Differenzialgleichung\n",
    "\n",
    "Die Differenzialgleichung für den harmonischen Oszillator besagt, dass für die gesuchte Funktion $u(t)$ folgendes gelten muss: Die zweite Ableitung der gesuchten Funktion nach der Zeit muss (bis auf einen Faktor $\\frac{g}{l}$) dem Negativen der gesuchten Funktion entsprechen. Wir suchen also eine Funktion $u(t)$, die bei zweimaliger Ableitung nach der Zeit bis auf das Vorzeichen und einen Vorfaktor unverändert ist."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Eine bekannt Funktion mit dieser Eigenschaft ist die Sinusfunktion $u(t)=\\sin(t)$, bei der im Allgemeinen noch die Amplitude $u_0$, die Kreisfrequenz $\\omega_0$ und eine Phasenverschiebung $\\varphi_0$ angegeben werden können:\n",
    "$$\n",
    "u(t)=u_0\\sin(\\omega_0 t+\\varphi_0).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Um zu prüfen, ob dieses $u(t)$ tatsächlich die Differenzialgleichung für den harmonischen Oszillator löst, setzen wir $u(t)$ in die Gleichung ein. Es ist\n",
    "\n",
    "$$\n",
    "\\frac{d^2u}{dt^2}(t)=-\\omega_0^2u_0\\sin(\\omega_0 t+\\varphi_0)\n",
    "$$\n",
    "und\n",
    "$$\n",
    "-\\frac{g}{l}u(t)=-\\frac{g}{l}u_0\\sin(\\omega_0 t+\\varphi_0)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Mit $\\omega_0=\\sqrt{\\frac{g}{l}}$ ist die Funktion $u(t)=u_0\\sin(\\omega_0 t+\\varphi_0)$ also tatsächlich eine Lösung für die Differenzialgleichung des harmonischen Oszillators. Für kleine Winkel $u$ schwingt ein Fadenpendel also wir ein harmonischer Oszillator mit der Periodendauer\n",
    "$$\n",
    "T=\\frac{2\\pi}{\\omega_0}=2\\pi\\sqrt{\\frac{l}{g}}.\n",
    "$$\n",
    "Die Werte für die Konstanten $u_0$ und $\\varphi_0$ sind durch zwei Anfangsbedingungen festgelegt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Gedämpftes Pendel\n",
    "\n",
    "### 1. Einfügung des Dämpfungsterms\n",
    "\n",
    "Bei einem realen Pendel gilt die Energieerhaltung nicht, da stets Verluste auftreten und zu einer Dämpfung führen. Der Energieverlust pro Zeit durch Dämpfung $\\frac{dE}{dt}(t)$ ist in vielen Fällen, etwa bei Luftreibung mit laminarer Strömung, proportional zum Quadrat der Geschwindigkeit $v(t)^2$\n",
    "\n",
    "$$\n",
    "\\frac{dE}{dt}(t)=-b\\,v(t)^2=-b\\left(\\frac{du}{dt}(t)\\cdot l\\right)^2\n",
    "$$\n",
    "\n",
    "mit einer Dämpfungskonstante $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Für die Energieänderung beim gedämpften Fadenpendel gilt also:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "-b\\left(\\frac{du}{dt}(t)\\cdot l\\right)^2 & \\stackrel{!}{=} & ml\\frac{du}{dt}(t)\\cdot\\left( g\\sin(u(t))+\\frac{d^2u}{dt^2}(t)\\cdot l\\right)\\\\\n",
    "\\Rightarrow -b\\frac{du}{dt}(t)\\cdot l & = & m\\cdot\\left( g\\sin(u(t))+\\frac{d^2u}{dt^2}(t)\\cdot l\\right)\\\\\n",
    "\\Rightarrow ml\\frac{d^2u}{dt^2}(t) & = & -mg\\sin(u(t))-bl\\frac{du}{dt}(t)\\\\\n",
    "\\Rightarrow \\frac{d^2u}{dt^2}(t) & = & -\\frac{g}{l}\\sin(u(t))-\\frac{b}{m}\\frac{du}{dt}(t)\\\\\n",
    "\\end{eqnarray}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Die Dämpfung führt also zum zusätzlichen Term $-\\frac{b}{m}\\frac{du}{dt}(t)$ in der Differenzialgleichung für das Fadenpendel. Dieser Term entspricht einer bremsenden Kraft deren Betrag proportional zur Geschwindigkeit des Fadenpendels ist."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Lösung für kleine Auslenkungen\n",
    "\n",
    "Die Differenzialgleichung für das gedämpfte Pendel lässt sich vereinfachen wenn nur kleine Auslenkungen aus der Ruhelage vorkommen. In dieser Kleinwinkelnäherung gilt wieder $\\sin(u(t))\\approx u(t)$. Damit vereinfacht sich die Differenzialgleichung für das gedämpfte Fadenpendel zu\n",
    "\n",
    "$$\n",
    "\\frac{d^2u}{dt^2}(t) = -\\frac{g}{l}u(t)-\\frac{b}{m}\\frac{du}{dt}(t)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Für diese Differentialgeleichung können Lösungen für unterchiedliche Fälle direkt angegeben werden:\n",
    "\n",
    "#### Kriechfall\n",
    "Für sehr große Dämpfung schwingt das Pendel gar nicht mehr sondern kriecht langsam in den Gleichgewichtszustand.\n",
    "\n",
    "[Anmerkung: Wenn $b$ sehr groß ist muss $\\frac{du}{dt}(t)$, also die Winkelgeschwindigkeit sehr klein sein.]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Schwingfall\n",
    "Gilt $\\frac{b}{2m}<\\sqrt{\\frac{g}{l}}$ so schwingt das Pendel noch und die Auslenkung aus der Ruhelage wird durch die Gleichung\n",
    "\n",
    "$$\n",
    "u(t)=u_0 e^{-\\frac{b}{2m}\\cdot t}\\cdot \\sin(\\omega_d t + \\varphi_0)\n",
    "$$\n",
    "\n",
    "gegeben. Dabei ist $\\omega_d = \\sqrt{\\frac{g}{l}-\\left(\\frac{b}{2m}\\right)^2}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Für kleine Winkel $u$ schwingt ein gedämpftes Fadenpendel also wir ein harmonischer Oszillator mit der Periodendauer\n",
    "\n",
    "$$\n",
    "T=\\frac{2\\pi}{\\omega_d}=\\frac{2\\pi}{\\sqrt{\\frac{g}{l}-\\left(\\frac{b}{2m}\\right)^2}}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Die Periodendauer vergrößert sich also durch die Dämpfung. Die Amplitude klingt gemäß $e^{-\\frac{b}{2m}\\cdot t}$ exponentiell mit der Zeit ab. Experimentell lässt sich die Stärke der Dämpfung aus der Verringerung der Amplitude mit der Zeit (Abfall auf $\\frac{1}{e}$ in der charakteristischen Zeit $\\tau=\\frac{2m}{b}$) bestimmen.\n",
    "\n",
    "Die Werte für die Konstanten $u_0$ und $\\varphi_0$ sind wieder durch zwei Anfangsbedingungen festgelegt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 3. Numerische Lösung für beliebige Auslenkungen\n",
    "### $\\to$ Umformulierung in System von Differenzialgleichungen 1. Ordnung\n",
    "\n",
    "Die Differenzialgleichung für das gedämpfte Fadenpendel für beliebig große Auslenkungen ist nicht einfach zu lösen. Die Lösungen sollen daher numerisch berechnet werden. Für die numerische Berechnung soll die Lösung (die unbekannte Funktion) am Ende als vektorwertige Funktion $\\vec{u}(t)$ ausgedrückt werden (den Grund dafür sehen wir gleich). Damit die Bezeichnung der unbekannten Funktion eindeutig bleibt benennen wir die gesuchte Funktion in der Differenzialgleichung für das Fadenpendel in $\\varphi(t)$ um:\n",
    "\n",
    "$$\n",
    "\\frac{d^2\\varphi}{dt^2}(t) = -\\frac{g}{l}\\sin(\\varphi(t))-\\frac{b}{m}\\frac{d\\varphi}{dt}(t)\n",
    "$$\n",
    "\n",
    "$\\varphi(t)$ ist steht also nun für den Winkel, um den das Pendel aus der Ruhelage ausgelenkt ist."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Nun wollen wir die nichtlineare Differenzialgleichung 2. Ordnung in ein System von 2 Differenzialgleichungen 1. Ordnung umformulieren. Dazu verwenden wir wie schon zuvor den Ausdruck für die Bahngeschwindigkeit\n",
    "\n",
    "$$\n",
    "v(t)=\\frac{d\\varphi}{dt}(t)\\cdot l\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Damit lassen sich beide Seiten der Differenzialgleichung für das gedämpfte Fadenpendel auch folgendermaßen schreiben:\n",
    "\n",
    "$$\n",
    "\\frac{1}{l}\\frac{dv}{dt}(t) = \\color{blue}{\\frac{d^2\\varphi}{dt^2}(t)  =  -\\frac{g}{l}\\sin(\\varphi(t))-\\frac{b}{m}\\frac{d\\varphi}{dt}(t)} =  -\\frac{g}{l}\\sin(\\varphi(t))-\\frac{b}{ml}v(t)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Zusammen mit der Definition von $v(t)$ haben wir nun 2. Differenzialgleichungen die das Fadenpendel beschreiben. Eine Gleichung für den \"Ort\" $\\varphi(t)$ (in diesem Fall der Winkel) und eine Gleichung für die \"Geschwindigkeit\" $v(t)$ (in diesem Fall die Bahngeschwindigkeit).\n",
    "Diese zwei Gleichungen können wir auch in Vektorform ausdrücken:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "Mit\n",
    "\n",
    "$$\n",
    "\\vec{u}(t) =\n",
    "\\left( \n",
    "\\begin{array}{c}\n",
    "\\varphi(t) \\\\ v(t)\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "und\n",
    "\n",
    "$$\n",
    "\\vec{f}(\\vec{u}(t)) = \\vec{f}(\\varphi(t),v(t)) = \n",
    "\\left( \n",
    "\\begin{array}{c}\n",
    " \\frac{1}{l}v(t) \\\\ \n",
    " -g\\sin(\\varphi(t))-\\frac{b}{m}v(t)\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "ergibt sich als Gleichung in Vektorform\n",
    "\n",
    "$$\n",
    "\\frac{d \\vec{u}(t)}{dt} = \\left( \n",
    "\\begin{array}{c}\n",
    "\\frac{d \\varphi}{dt}(t) \\\\ \\frac{d v}{dt}(t)\n",
    "\\end{array}\n",
    "\\right)\n",
    "= \\vec{f}(\\vec{u}(t))\n",
    "$$\n",
    "\n",
    "Diese Gleichung werden wir numerisch lösen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Physikalisches Pendel\n",
    "\n",
    "### 1. Herleitung der Differenzialgleichung\n",
    "\n",
    "Bei einem realen Fadenpendel hat die Pendelmasse eine endliche Ausdehnung und auch der Faden hat eine zusätzliche Masse. Werden diese Punkte im Modell des Pendels berücksichtigt so spricht man von einem physikalischen Pendel. Das Modell des physikalischen Pendels ist zudem geeignet, beliebige Gemometrien eines Pendels zu beschreiben.\n",
    "\n",
    "<center>\n",
    "<img src=\"physikalisches pendel.JPG\" alt=\"Drawing\" style=\"width: 21%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Wir betrachten einen beliebigen starren Körper mit Volumen $V$ und ortsabhängiger Dichte $\\rho(\\vec{r})$. Dieser Körper soll um eine beliebige Rotationsachse durch den Körper schwingen. Im Gleichgewicht befindet sich der Schwerpunkt des Körpers im Abstand $l$ unter der Rotationsachse. Bei der Schwingung wird der Körper um den Winkel $u(t)$ aus der Gleichgewichtslage ausgelenkt. Die potentielle Energie des Körpers ist dann\n",
    "\n",
    "$$\n",
    "E_{\\rm{pot}}(t)=mgl\\left(1-\\cos(u(t))\\right).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Die Rotationsenergie bei Rotation um diese Achse ist\n",
    "\n",
    "\\begin{eqnarray}\n",
    "E_\\rm{rot}& = &\\int\\limits_V \\frac{1}{2}\\left(v(\\vec{r},t)\\right)^2\\,dm=\\frac{1}{2}\\int\\limits_V \\left(v(\\vec{r},t)\\right)^2\\rho(\\vec{r})\\,dV=\\frac{1}{2}\\int\\limits_V \\left(|\\vec{r_{\\perp}}|\\frac{du}{dt}(t)\\right)^2\\rho(\\vec{r})\\,dV \\\\ & = & \\frac{1}{2}\\int\\limits_V |\\vec{r_{\\perp}}|^2\\left(\\frac{du}{dt}(t)\\right)^2\\rho(\\vec{r})\\,dV = \\frac{1}{2}\\underbrace{\\int\\limits_V |\\vec{r_{\\perp}}|^2\\rho(\\vec{r})\\,dV}_{=:J}\\left(\\frac{du}{dt}(t)\\right)^2=\\frac{1}{2}J \\left(\\frac{du}{dt}(t)\\right)^2\n",
    "\\end{eqnarray}\n",
    "\n",
    "Dabei ist\n",
    "\n",
    "$J=\\int\\limits_V |\\vec{r}_{\\perp}|^2 \\rho(\\vec{r})\\,dV$ das Trägheitsmoment des Körpers\n",
    "\n",
    "\n",
    "$\\vec{r}_{\\perp}$ der Abstand eines Volumenelements senkrecht zur Drehachse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Der Energieverlust pro Zeit durch Dämpfung $\\frac{dE}{dt}(t)$ soll wie zuvor proportional zum Quadrat der Geschwindigkeit $v(t)^2$ sein. Dann gilt\n",
    "\n",
    "$$\n",
    "\\frac{dE}{dt}(t)=-b\\,v(t)^2=-b\\left(\\frac{du}{dt}(t)\\cdot l\\right)^2\\stackrel{!}{=} mgl \\sin(u(t))\\,\\frac{du}{dt}(t)+J\\,\\frac{du}{dt}(t)\\,\\frac{d^2u}{dt^2}(t)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Daraus folgt als Differenzialgleichung für die gedämpfte Schwingung eines Körpers\n",
    "\n",
    "$$\n",
    "\\frac{d^2u}{dt^2}(t)=-\\frac{mgl}{J} \\sin(u(t))-\\frac{bl^2}{J}\\frac{du}{dt}(t)\n",
    "$$\n",
    "\n",
    "Wir erhalten also eine Differenzialgleichung wie beim gedämpften Fadenpendel, lediglich die Vorfaktoren bekommen eine andere Bedeutung."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Für kleine Winkel $u$ schwingt ein gedämpftes physikalisches Pendel folglich mit der Periodendauer\n",
    "\n",
    "$$\n",
    "T=\\frac{2\\pi}{\\omega_d}=\\frac{2\\pi}{\\sqrt{\\frac{mgl}{J}-\\left(\\frac{bl^2}{2J}\\right)^2}}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "[Anmerkung: Der Ausdruck für die Dämpfung gilt streng genommen nur, wenn die Oberflächengeschwindigkeit des Körpers für alle Punkte auf der Oberfläche gleich ist.]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Übung:\n",
    "\n",
    "Berechnen Sie für ein Fadenpendel mit Länge $l$ und Masse $m$ das Trägheitsmoment $J$.\n",
    "\n",
    "Prüfen Sie, ob Sie mit diesem Trägheitsmoment wieder die Differenzialgleichung für das gedämpfte Fadenpendel erhalten."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Ausblick\n",
    "\n",
    "Schon die Berücksichtigung der Dämpfung beim physikalischen Pendel macht deutlich, dass für komplexere Geometrien neue Anforderungen an die Modellierung entstehen. Für ein physikalisches Pendel sind insbesondere folgende Aspekte relevant:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "\n",
    "- reale Körper sind nicht starr sondern verformen sich unter dem Einfluss von Gravitations- und Trägheitskräften ($\\to$ Turbinen, Schwungradspeicher, space elevator)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- die Aufhängung eines realen Pendels ist nicht starr sondern gibt nach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- unterschiedliche Oberflächengeschwindigkeiten führen zu unterschiedlicher Dämpfung ($\\to$ Winkraftanlagen)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- durch Lagerreibung entstehen weitere Verluste"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Für die Modellierung realer System werden Körper daher häufig in einzelne Volumenelemente (\"finite Elemente\") zerlegt. Für diese Elemente werden dann die von außen wirkenden Kräfte und die Kräfte zwischen den Elementen ($\\to$ Kontinuumsmechanik) berücksichtigt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Experiment\n",
    "\n",
    "Das im Experiment genutze physikalische Pendel kann in guter Näherung als Kombination zweier Zylinder angesehen werden. Mit den jeweiligen Massen folgt für das Trägheitsmoment \n",
    "\n",
    "$$\n",
    "J_{\\rm Pendel}=1,08\\, \\rm{kg\\cdot m^2}\n",
    "$$\n",
    "\n",
    "und für den Abstand des Schwerpunkts von der Rotationsachse\n",
    "\n",
    "$$\n",
    "l=0,96\\,\\rm{m}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "#### Aufgabe:\n",
    "\n",
    "- Betrachten Sie zunächst kleine Auslenkungen aus der Ruhelage. Vergleichen Sie das Verhalten des Pendels im Experiment und in der Simulation mit der analytischen Näherungslösung.\n",
    "- Untersuchen Sie nun des Verhalten des Pendels bei größeren Auslenkungen. Vergleichen Sie wieder das Verhalten des Pendels im Experiment und in der Simulation. Welche Änderungen ergeben sich gegenüber kleinen Auslenkungen?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
