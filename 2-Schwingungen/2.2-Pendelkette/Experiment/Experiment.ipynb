{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Pendelkette\n",
    "\n",
    "## Geometrie des Systems\n",
    "\n",
    "Wir betrachten eine Kette aus $N$ Oszillatoren, die Drehschwingungen auf einer gemeinsamen Achse ausführen können. Jeder Oszillator habe das Trägheitsmoment $J$. Je zwei benachbarte Oszillatoren sind über eine Feder mit Direktionsmoment $D^*$ gekoppelt. Die Auslenkung der Oszillatoren aus ihrer jeweiligen Ruhelage ist durch den Winkel $u_i(t)$ gegeben.\n",
    "\n",
    "<center>\n",
    "<img src=\"Pendelkette.jpg\" alt=\"Drawing\" style=\"width: 60%;\"/>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Physikalische Grundgesetze\n",
    "\n",
    "Zur Herleitung der Differenzialgleichung für die Pendelkette betrachten wir die Drehmomente, die auf einen Oszillator wirken. Da die Oszillatoren $1$ und $N$ jeweils nur einen Nachbarn haben werden sie gesondert betrachtet. Auf jeden anderen Oszillator $i$ mit $1<i<N$ wirken drei Drehmomente $M_i$:\n",
    "- Das Drehmoment durch die Kopplung zum Oszillator $i+1$\n",
    "\n",
    "$$\n",
    "M_i^{i+1}=-D^*(u_i-u_{i+1})\n",
    "$$\n",
    "\n",
    "- Das Drehmoment durch die Kopplung zum Oszillator $i-1$\n",
    "\n",
    "$$\n",
    "M_i^{i-1}=-D^*(u_i-u_{i-1})\n",
    "$$\n",
    "\n",
    "- Das Drehmoment aufgrund von winkelgeschwindigkeitsproportionaler Dämpfung mit der Dämpfungskonstante $k$ \n",
    "\n",
    "$$\n",
    "M_i^{\\rm{Dämpfung}}=-k\\frac{du_i}{dt}(t)\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Aus der Bewegungsgleichung für Drehbewegungen $M=J\\frac{d^2u}{dt^2}(t)$ für Körper mit dem Trägheitsmoment $J$ folgt dann für jeden Oszillator\n",
    "\n",
    "\\begin{eqnarray}\n",
    "\\frac{d^2u_i}{dt^2}(t) & = & \\frac{\\Sigma\\,M_i}{J}\\\\\n",
    "& = & \\frac{1}{J}\\left(M_i^{i+1}+M_i^{i-1}+M_i^{\\rm{Dämpfung}}\\right)\\\\\n",
    "& =& \\frac{1}{J}\\left(-D^*\\left(u_i-u_{i+1}+u_i-u_{i-1}\\right)-k\\frac{du_i}{dt}(t)\\right)\\\\\n",
    "& =& \\frac{D^*}{J}\\left(u_{i+1}-2u_i+u_{i-1}\\right)-\\frac{k}{J}\\frac{du_i}{dt}(t)\n",
    "\\end{eqnarray}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Mit der Winkelgeschwindigkeit $v_i(t)=\\frac{du_i}{dt}(t)$ wird die Bewegung jedes Oszillators $1<i<N$ durch folgende Differenzialgleichungen beschrieben:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "v_i(t) & = & \\frac{du_i}{dt}(t)\\\\\n",
    "\\frac{dv_i}{dt}(t) & = & \\frac{D^*}{J}\\left(u_{i+1}-2u_i+u_{i-1}\\right)-\\frac{k}{J}v_i(t)\n",
    "\\end{eqnarray}\n",
    "\n",
    "Diese Differenzialgleichungen werden wir numerisch lösen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Experiment zu Pendelkette\n",
    "\n",
    "### Bestimmung der Direktionsmoments und der Dämpfungskonstante\n",
    "\n",
    "Zunächst sollen die Systemparamter $D^*$, $k$ und $J$ experimentell bestimmt werden. Da das Direktionsmoment $D^*$ und die Dämfungskonstante $k$ in gleicher Weise mit dem Trägheitsmoment $J$ skalieren können wir $J$ in diesem Experiment nicht bestimmen. Daher setzen wir der Einfachheit halber $J=1$.\n",
    "\n",
    "#### Aufgabe:\n",
    "Bestimmen Sie experimentell Werte für das Direktionsmoment $D^*$ und die Dämpfungskonstante $k$.\n",
    "\n",
    "- Regen Sie den ersten Oszillator von Hand pulsartig an und messen Sie die Dauer, die die Störung benötigt, um über die Pendelkette zu laufen.\n",
    "- Wählen Sie in der Simulation $k=0$. Passen Sie nun in der Simulation $D^*$ so an, dass Sie die experimentelle Laufzeit erreichen. Nutzen Sie dazu in der Simulation für die Anregung des ersten Oszillators folgenden Code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def AnregungLinks(t):\n",
    "    if t<.50/2:\n",
    "        return 1*sin(2*pi/0.50*t)\n",
    "    else:\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- Regen Sie den ersten Oszillator erneut von Hand pulsartig an (Auslenkung aus der Ruhelage um etwa $45^°$) und beobachten Sie, wie häufig die Störung auf der Pendelkette hin- und her läuft, bis die Amplitude soweit gedämpft ist dass sie nicht mehr sichtbar ist.\n",
    "- Passen Sie nun in der Simulation $k$ so an, dass Sie die experimentelle Beobachtung reproduzieren können."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Untersuchung des Schwingungsverhaltens der Pendelkette\n",
    "\n",
    "Bei korrekter Bestimmung von $D^*$ und $k$ sollten Sie charakteristische Zustände der Pendelkette bei periodischer Anregung, etwa Resonanzen oder das Auftreten stehender Wellen, in der Simulation reproduzieren können.\n",
    "\n",
    "#### Aufgabe:\n",
    "\n",
    "- Regen Sie den linken Oszillator der Pendelkette mit dem Motor periodisch an. Variieren Sie die Anregungsfrequenz so, dass sie einen charakteristischen Zustand der Pendelkette erreichen. Notieren Sie für den Zustand die Position von Schwingungsbäuchen oder -knoten.\n",
    "- Prüfen Sie, ob Sie in der Simulation mit den vorher bestimmten Werten für $D^*$ und $k$ das experimentelle Verhalten reproduzieren können. Nutzen Sie dazu in der Simulation für die periodische Anregung des ersten Oszillators folgenden Code mit angepasster Periodendauer der Anregung (hier 2,9 Sekunden):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def AnregungLinks(t):\n",
    "    return 1*sin(2*pi/2.9*t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Prüfen Sie, ob Sie mit Ihrer Simulation auch Vorhersagen zum Verhalten der Pendelkette treffen können. Variieren Sie dazu in der Simulation die Anregungsparameter (Anregungsfrequenz, Anregung auf einer Seite oder auf beiden Seiten, bei beidseitiger Anregung gleichphasige oder gegenphasige Anregung) um einen charakteristische Zustände der Pendelkette zu erhalten.\n",
    "- Führen Sie das Experiment mit entsprechenden Parametern durch und vergleichen Sie die Resultate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "#### Referenzwerte:\n",
    "\n",
    "Bei pulsartiger Anregung des ersten Oszillators benötigt die Welle ca. 1,8 Sekunden um über alle 48 Oszillatoren zu laufen.\n",
    "\n",
    "Bei pulsartiger Anregung des 1. Oszillators um ca. 45° (beide Enden frei) läuft die Welle ca. 3 Mal hin und her bis die Amplitude so weit gedämpft ist dass sie nicht mehr sichtbar ist. Die Zeit dafür beträgt ca. 11,8 Sekunden.\n",
    "\n",
    "Diese Verhalten wird erreicht für $D^*\\approx 600$ und $k\\approx 0,6$ (für das normierte Trägheitsmoment $J=1$).\n",
    "\n",
    "Für sinusförmige Anregung links, rechtes Ende frei finden sich im Experiment folgende charakteristische Periodendauern $T$:\n",
    "- Bei $T\\approx 2,05\\,$s Resonanz mit einem Knotenpunkt bei Oszillator Nr. 31\n",
    "- Bei $T\\approx 1,60\\,$s stehende Welle zwei Knotenpunkt bei Oszillatoren mit  Nr. 12 und 36 ($\\lambda$ auf Pendelkette)\n",
    "- Bei $T\\approx 1,40\\,$s Resonanz mit zwei Knotenpunkten bei Oszillatoren Nr. 19 und 38\n",
    "- Bei $T\\approx 1,13\\,$s stehende Welle mit drei Knotenpunkt bei Oszillatoren mit  Nr. 11, 27 und 39 ($\\frac{3}{2}\\lambda$ auf Pendelkette)\n",
    "- Bei $T\\approx 1,05\\,$s Resonanz mit drei Knotenpunkt bei Oszillatoren mit  Nr. 13, 28 und 40 (etwas mehr als $\\frac{3}{2}\\lambda$ auf Pendelkette)\n",
    "- Bei $T\\approx 0,91\\,$s stehende Welle mit vier Knotenpunkt bei Oszillatoren mit  Nr. 7, 19, 32 und 42 ($2\\lambda$ auf Pendelkette)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
